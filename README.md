The MSeg Motion Segmentation Evaluation Framework
===

**MSeg** is a framework for the [ArmarX](https://armarx.humanoids.kit.edu/index.html) robot development environment,
built to run and evaluate motion segmentation algorithms written in C++, Java, Python or MATLAB. Please refer to the
[MSeg documentation](https://mseg.readthedocs.io/) for guides on how to install the framework and how to get started.

If you encounter a problem, feel free to open a new issue in the
[issue tracker](https://gitlab.com/h2t/kit-mseg/mseg/issues) or send an email to
<a href="mailto:incoming+h2t/kit-mseg/mseg@gitlab.com">incoming+h2t/kit-mseg/mseg@gitlab.com</a>
(this will create an issue in the issue tracker).

--- *The rest of this document is relevant for developers only* ---


# MSeg Documentation

This repository contains the source code for the official documentation on
[mseg.readthedocs.io](https://mseg.readthedocs.io).


## Writing documentation

This documentation is written in [reStructuredText](http://docutils.sourceforge.net/rst.html) and deployed to
[Read the Docs](https://readthedocs.org). Apart from that, [Sphinx](http://www.sphinx-doc.org/en/stable/) is used.

To work with the documentation locally, several dependencies need to be installed. For Debian or its derivatives:

```shell
make depsdeb | xargs sudo apt install
make depspip3 | xargs sudo pip3 install
```

You can also run `make depspip3` to get the list of Pip3 dependencies.


### Building the documentation

Building the HTML documentation requires invoking sphinx. The repository includes a makefile to ease the invokation.
Running `make install` should build the documentation into `./build/html`. The root document is `./build/html/index.html`
which can be opened with a browser.


### Prefered workflow

To avoid building the documentation manually after each change, it is advised to make use of `sphinx-autobuild`.
Invoking `make watch` in the root of the documentation repository will watch for changes
in `./source`. If a change was detected, the build process will be started and the changes will be deployed to 
`./build/html`.

`sphinx-autobuild` also starts a development webserver (default on localhost:8000), which will refresh automatically
if changes where detected.


# License

Licensed under the [GPL 2.0 License](./LICENSE.md).
