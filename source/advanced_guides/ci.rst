Notes on Continuous Integration
===============================

For the continuous integration, the `MSeg Umbrella Repository <https://gitlab.com/h2t/kit-mseg/mseg>`_
can be utilised, especially the bash scripts, which will be discussed after the general
information.


General
-------

For each MSeg Core Module version (i.e. for each tag in the Git repository ``core``),
a new branch in the umbrella repository will be created. For each patch in one of the
subprojects, the corresponding branch will be updated (i.e. push events on that branch).

The convention is as follows: If the current version of the MSeg Core Module is v1.2.0,
there will be a branch "1.2.0" in the umbrella repository. If a patch for one of the
subprojects is released, that specific release will be updated in the ``stable-refs.cfg``
file.

Any events on the ``master`` branch must be ignored.


Provided Script Files
---------------------

The provided scripts of the MSeg Umbrella Repository are located in the ``./scripts``
folder. However, they should be called from within the root directory of the umbrella
repository, due to the way bash resolves paths.


``./scripts/clone-repos.sh``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Clones all subprojects into the expected locations relative to the umbrella repositories
root.


``./scripts/change-to-stable-refs.sh``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Uses the config file ``./stable-refs.cfg`` to change to the refs known to be stable and
work well with each other.


``./scripts/submake.sh``
^^^^^^^^^^^^^^^^^^^^^^^^

This script helps to execute a make rule on all subprojects. It fails gracefully if the
make rule does not exist one or more of the subprojects (i.e. it ignores it).

The script expects an input parameter which is the rule to be executed on all subprojects.

Rules which are usually called are the following: 

- ``.depsdeb``: Running ``bash ./scripts/submake.sh .depsdeb`` yields the Debian dependencies
  of all subprojects combined. Running this command will result in an unordered list with
  potential duplicate entries, with the individual entries being separated by new lines ``\n``.
  Therefore it is recommended to pipe the result of this rule into ``sort -u``, which sorts
  the list and gets rid of all duplicate entries: ``bash ./scripts/submake.sh .depsdeb | sort -u``.
- ``.depspip`` and ``.depspip3``: Conceptually the same as ``.depsdeb``, but these rules yield
  the Python deps (in ``pip``) and the Python3 deps (in ``pip3``) respectively.
- ``.bashrc``: Returns the entries of each subproject which should be added to the ``.bashrc``
  file. Due to the fact that most of those entries are for convenience only, this rule is
  irrelevant for the CI setup, with the exception of required environment variables.
  See environment variables for this purpose.
- ``install``: Installs all subprojects, however this installation looks like. The responsibility
  for installing the project correctly is held by the corresponding ``makefile`` in the
  subprojects root directory.
- ``package``: Packages the subprojects, which yields ``.deb`` releases in the ``deb-dist``
  folder in each subproject. The ``makefile`` of the corresponding subproject is responsible
  for a proper packaging. After running this rule, all ``.deb`` releases can be collected by
  running ``mkdir ./deb-dist`` ``&&`` ``mv **/deb-dist/*.deb ./deb-dist/``. Please note:
  the ``package`` rule does not require that the ``install`` rule was ran in advance.


Other scripts
^^^^^^^^^^^^^

All other scripts are not relevant or not suitable for CI pipelines.


Environment Variables
---------------------

Some projects require that certain environment variables are set. Some of the following
environment require that the root directory of the MSeg Umbrella Repository is set as
``MSEG_DIR``. The name of this environment variable, however, is changable. Change the
exorts accordingly if the environment variable has a different name on your system.


``CLASSPATH``
^^^^^^^^^^^^^

With ``MSEG_DIR`` being the root directory of the MSeg Umbrella Repository, the Java
classpath needs to be set as follows:

.. code-block:: bash

    export CLASSPATH="${CLASSPATH}:${MSEG_DIR}/core-ice/buildjava/build/lib/mseginterfacesjava.jar"
    export CLASSPATH="${CLASSPATH}:${MSEG_DIR}/pli-java/build/lib/plijava.jar"

Other than classpath or other environment variables, Java unfortunately does not support a standard
way of resolving local dependencies.


``MSEG_PLI_MATLAB_DIR``
^^^^^^^^^^^^^^^^^^^^^^^

The MATLAB PLI tries to install ``.m`` libraries. If the ``MSEG_PLI_MATLAB_DIR`` is not
set, it will try to install those libraries in system paths where it has no access to.
With ``MSEG_DIR`` being the root directory of the MSeg Umbrella Repository:

.. code-block:: bash

    export MSEG_PLI_MATLAB_DIR="${MSEG_DIR}/pli-matlab"


``MSEG_TOOLS_DIR``
^^^^^^^^^^^^^^^^^^^^^^^

MSeg tools tries to install templates to use with the generator. If the ``MSEG_TOOLS_DIR``
is not set, it will try to install those libraries in system paths where it has no access to.
With ``MSEG_DIR`` being the root directory of the MSeg Umbrella Repository:

.. code-block:: bash

    export MSEG_TOOLS_DIR="${MSEG_DIR}/mseg-tools"


``THREADS``
^^^^^^^^^^^

While this environment variable is not mandatory, it can drastically speed up the pipeline.
Set it to the number of cores available.

Example for a quad-core processor:

.. code-block:: bash

    export THREADS=4

Currently, this environment variable is only used in C++ builds, but in the future it may be
utilised in Java builds, too. Building independend subprojects in parallel may be possible as well.
