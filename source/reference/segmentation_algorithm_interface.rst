``SegmentationAlgorithm`` Interface
===================================

All PLIs offer the class ``SegmentationAlgorithm`` which can be derived in order to communicate with the core module.


Properties
----------

Properties of ``SegmentationAlgorithm``.


``data``
^^^^^^^^

    Type:
        :doc:`DataExchangeProxy <./data_exchange_proxy_interface>`
    Description
        Proxy to exchange data between the PLI and the core module. This property is initialised by the corresponding
        PLI and must not be overwritten.


``name``
^^^^^^^^

    Type:
        String
    Description:
        The name of the algorithm. The name must not be empty and must be alphanumeric and start with a character.
        Set this property in the constructor.


``requiresTraining``
^^^^^^^^^^^^^^^^^^^^

    Type:
        bool
    Default:
        false
    Description:
        Flag to indicate whether the algorithm should be trained.
        Set this property in the constructor.

``trainingGranularity``
^^^^^^^^^^^^^^^^^^^^^^^

    Type:
        Granularity
    Default:
        Granularity.Medium
    Description:
        Used to filter the training ground truth data for the set granularity.
        Possible values are ``Granularity.Fine``, ``Granularity.Medium``, and ``Granularity.Rough``.
        Set this property in the constructor.
        This property is only considered if ``requiresTraining`` is set to true.


Methods
-------

Methods of ``SegmentationAlgorithm``.


``SegmentationAlgorithm()`` or ``__init__()`` or ``constructor.m``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        ``SegmentationAlgorithm`` ()
    Description:
        Constructor.
        Set properties like ``name``, ``requiresTraining`` or ``trainingGranularity`` here.
        Set algorithm parameters here.


``train()`` or ``train.m``
^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``train`` ()
    Return:
        void
    Description:
        This method will be called for each motion recording in the training dataset. You can fetch the current
        motion recording here using the ``data`` property and use it to train your variables.


``resetTraining()`` or ``reset_training.m``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``resetTraining`` ()
    Return:
        void
    Description:
        This method will be called after one part of the cross validation was executed. Make sure to
        reset all trained variables to not falsify the evaluation.


``segment()`` or ``segment.m``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``segment`` ()
    Return:
        void
    Description:
        This method will be called for each motion recording in the testing dataset.


``registerBoolParameter()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``registerBoolParameter`` (String ``name``, String ``description``, bool ``defaultValue`` )
    Return:
        void
    Description:
        Registers a boolean parameter ``name`` with a default value of ``defaultValue`` to tweak in the GUI.


``registerIntParameter()``
^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``registerIntParameter`` (String ``name``, String ``description``, int ``defaultValue``, int
        ``minimumValue`` = INT_MIN, int ``maximumValue`` = INT_MAX)
    Return:
        void
    Description:
        Registers an int parameter ``name`` with a default value of ``defaultValue`` to tweak in the GUI. The allowed
        range of values can be controlled with ``minimumValue`` and ``maximumValue``.


``registerFloatParameter()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``registerFloatParameter`` (String ``name``, String ``description``, float ``defaultValue``,
        int ``decimals`` = 2, float ``minimumValue`` = -FLOAT_MAX, float ``maximumValue`` = FLOAT_MAX)
    Return:
        void
    Description:
        Registers a float parameter ``name`` with a default value of ``defaultValue`` to tweak in the GUI. The allowed
        range of values can be controlled with ``minimumValue`` and ``maximumValue``. The precision (number of decimals)
        can be controlled with ``decimals``.


``registerStringParameter()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``registerStringParameter`` (String ``name``, String ``description``, String ``defaultValue`` )
    Return:
        void
    Description:
        Registers a String parameter ``name`` with a default value of ``defaultValue`` to tweak in the GUI.


``registerJsonParameter()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        void ``registerJsonParameter`` (String ``name``, String ``description``, JSON ``defaultValue`` )
    Return:
        void
    Description:
        Registers a JSON parameter ``name`` with a default value of ``defaultValue`` to tweak in the GUI.


``getBoolParameter()``
^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        bool ``getBoolParameter`` (String ``name`` )
    Return:
        bool
    Description:
        Get a registered bool parameter by its ``name``.


``getIntParameter()``
^^^^^^^^^^^^^^^^^^^^^

    Signature:
        int ``getIntParameter`` (String ``name`` )
    Return:
        int
    Description:
        Get a registered int parameter by its ``name``.


``getFloatParameter()``
^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        float ``getFloatParameter`` (String ``name`` )
    Return:
        float
    Description:
        Get a registered float parameter by its ``name``.


``getStringParameter()``
^^^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        String ``getStringParameter`` (String ``name`` )
    Return:
        String
    Description:
        Get a registered String parameter by its ``name``.


``getJsonParameter()``
^^^^^^^^^^^^^^^^^^^^^^

    Signature:
        JSON ``getJsonParameter`` (String ``name`` )
    Return:
        JSON
    Description:
        Get a registered JSON parameter by its ``name``.
