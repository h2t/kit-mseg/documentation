The ``mseg*`` Terminal Tools
============================

MSeg ships with three terminal tools, namely:

- ``msegcm`` Control the MSeg core module
- ``msegdata`` Manage the datasets
- ``mseggen`` Help setting up new motion segmentation algorithm projects

.. note:: You can run ``<command> --help`` at any time to get a quick overview on the usage and synopsis. If you need
    more detail for a given subcommand, you can run ``<command> <subcommand> --help`` instead. *The usage of the tools
    may change with future versions of MSeg*. If the information on this page is contradicting the information given
    from the ``--help``, *always rely on that help*.


``msegcm`` Tool
---------------

With ``msegcm``, the MSeg core module can be controlled.


Available subcommands
^^^^^^^^^^^^^^^^^^^^^

- ``status``: Prints the status of the MSeg core module processes
- ``start``: Start the MSeg core module processes
- ``stop``: Stop the MSeg core module processes
- ``restart``: Stop and start the MSeg core module processes

If an unexpected error leads to the crash of one or more processes, you will need to restart the MSeg core module using
``msegcm restart``. A crash can be diagnosed if ``msegcm status`` indicates that the status of the MSeg core module is
either "mixed" or "stopped".


.. note:: The MSeg core module cannot start properly if ArmarX is not running. You can query the status of ArmarX by
    running ``armarx status``, and you can start it by running ``armarx start``.


``msegdata`` Tool
-----------------

With ``msegdata``, the datasets available with MSeg can be managed. The datasets are not included by default because
newer datasets may still be useful for older versions of MSeg. The datasets are versioned separately for that reason.


Available subcommands
^^^^^^^^^^^^^^^^^^^^^

- ``fetch``: Fetches the datasets and stores them locally into ``~/.mseg/datasets`` (This is a Git repository). This
  subcommand **must** be ran once to initialise the datasets (see :doc:`quickstart guide <../guides/quickstart>`)
- ``update``: Updates the local datasets to the newest versions. Old versions will remain untouched

.. note:: If you are experienced with Git, then please note that ``msegdata`` is merely a façade for the corresponding
    Git repository (``~/.mseg/datasets``) of the datasets. If you prefer, you can use Git in first place instead of
    ``msegdata``. You are *still* advised to at least run ``msegdata fetch`` to clone the Git repository into the
    correct location.


``mseggen`` Tool
----------------

The ``mseggen`` command is a helper tool to kick-start a new motion segmentation algorithm project. It creates all
source files (and build files, if appropriate) needed for a given programming language with well-commented stubs and
dummy-implementations.


Synopsis
^^^^^^^^

``mseggen [--base-path BASE_PATH] [--train] (--cpp | --python | --java | --matlab) <project-name>``

- The option ``--base-path`` can be used to set the base path in which the project should be created. It defaults to
  the current working directory
- The flag ``--train`` is optional. If set, the code will be included to use the train API. If not set, those parts
  will be commented out
- The flags ``--cpp``, ``--python``, ``--java`` and ``--matlab`` are mutually exclusive. Exactly one of them must be
  set
- ``<project-name>`` is the name of the project, respectively the motion segmentation algorithm. It must me
  alphanumeric and start with a character


Examples
^^^^^^^^

- To implement the PCA approach by Barbič in Java the usage would be: ``mseggen --java PCABarbic``
- To implement a machine learning algorithm in Python, located in ``~/projects``:
  ``mseggen --base-path ~/projects --train --java MLApproach``

