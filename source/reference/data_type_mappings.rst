Data Type Mappings
==================

Data types used in this documentation map to different data structures or constructs depending on the used
programming language. The following table shows the corresponding mappings.

.. raw:: html

    <table class="docutils">
    <thead>
    <tr><th>Language</th>
    <th>bool</th>
    <th>int</th>
    <th>float</th>
    <th>String</th>
    <th>List</th>
    <th>JSON</th>
    </tr>
    </thead>
    <tbody>
    <tr><td>C++</td>
    <td>bool</td>
    <td>int</td>
    <td>double</td>
    <td>std::string</td>
    <td>std::vector</td>
    <td><a href="https://open-source-parsers.github.io/jsoncpp-docs/doxygen/index.html">Json::Value</a></td>
    </tr>
    <tr><td>Java</td>
    <td>boolean</td>
    <td>int</td>
    <td>double</td>
    <td>String</td>
    <td>Array</td>
    <td><a href="https://github.com/stleary/JSON-java">org.json.JSONObject</a></td>
    </tr>
    <tr><td>Python</td>
    <td>bool</td>
    <td>int</td>
    <td>float</td>
    <td>str</td>
    <td>list</td>
    <td><a href="https://docs.python.org/2/library/json.html">json</a></td>
    </tr>
    <tr><td>Java</td>
    <td>bool</td>
    <td>int</td>
    <td>float</td>
    <td>char</td>
    <td>list</td>
    <td><a href="https://docs.python.org/2/library/json.html">py.json</a></td>
    </tr>
    </tbody>
    </table>
