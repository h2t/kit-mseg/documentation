Integrate a Python Algorithm
============================

This guide will show you how to setup your workspace to integrate your own Python motion segmentation algorithm using
the Python PLI. It is assumed that you either followed the :doc:`quickstart guide <./quickstart>` or you
:doc:`compiled MSeg from source <../advanced_guides/compile_mseg>`.


Install the Skeleton Project
----------------------------

We will use the ``mseggen`` terminal tool to install a skeleton project.

.. code-block:: bash

    # Define the base directory where the project directory should be located
    $ export $PROJECT_BASE_DIR=~/projects
    # Run the generation tool to create the skeleton.
    # If your algorithm requires training, add the --train flag
    $ mseggen -b $PROJECT_BASE_DIR --python ExampleAlgorithm


Run the Algorithm
-----------------

To run the algorithm, both ArmarX and the MSeg core module must be running. You can then start the algorithm like this:

.. code-block:: bash

    # Change into the project folder
    $ cd $PROJECT_BASE_DIR/examplealgorithm
    # Run the algorithm
    $ python examplealgorithm.py

You should now see an output like this:

.. code-block:: bash

    user@machine:~/projects/examplealgorithm$ python examplealgorithm.py
    Connecting to MSeg core module...
    Connection established
    ExampleAlgorithm ready

You can stop the algorithm with :kbd:`Ctrl` + :kbd:`C` at any time.


How to Proceed
--------------

You can now start implementing your algorithm. You may also want to get more familiar with the
:doc:`SegmentationAlgorithm API reference <../reference/segmentation_algorithm_interface>`, the
:doc:`MSeg GUI <../reference/mseg_gui>` and the
:doc:`msegcm, msegdata, and mseggen terminal tools <../reference/mseg_terminal_tools>`.
