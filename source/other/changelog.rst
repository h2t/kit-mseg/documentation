Changelog
=========


Version 1.2
-----------

**Bugfixes**

- Fix a CMake dependency problem (`core#81 <https://gitlab.com/h2t/kit-mseg/core/issues/81>`_)
- Enable evaluation result export button only if results arrive (`core#83 <https://gitlab.com/h2t/kit-mseg/core/issues/83>`_)

**Features**

- Support proper packaging and supply binaries (`core#82 <https://gitlab.com/h2t/kit-mseg/core/issues/82>`_)
- Add MSeg icon to GUI (`core#84 <https://gitlab.com/h2t/kit-mseg/core/issues/84>`_)


Version 1.1
-----------

**Features**

- Implement parameter save/load (`core#4 <https://gitlab.com/h2t/kit-mseg/core/issues/4>`_)
- Allow exporting evaluation results as XML (`core#70 <https://gitlab.com/h2t/kit-mseg/core/issues/70>`_)

**Bugfixes**

- Load motions in the background (`core#65 <https://gitlab.com/h2t/kit-mseg/core/issues/65>`_)
- Disable segment current button (not yet implemented) (`core#67 <https://gitlab.com/h2t/kit-mseg/core/issues/67>`_)
- Fix a bug where motions are loaded twice (`core#68 <https://gitlab.com/h2t/kit-mseg/core/issues/68>`_)
- Disable segment and evaluate buttons while a new dataset ist being opened (`core#71 <https://gitlab.com/h2t/kit-mseg/core/issues/71>`_)
- Fix an issue where thousands separators could cause crashes (`core#75 <https://gitlab.com/h2t/kit-mseg/core/issues/75>`_)
- Fix issues with a more recent version of MMMTools (`core#80 <https://gitlab.com/h2t/kit-mseg/core/issues/80>`_)


Version 1.0
-----------

**Bugfixes**

- Fix an unimplemented method preventing Java PLI from compiling (`core#30 <https://gitlab.com/h2t/kit-mseg/core/issues/30>`_, `core#31 <https://gitlab.com/h2t/kit-mseg/core/issues/31>`_)
- Fix MATLAB dependency issues (`core#56 <https://gitlab.com/h2t/kit-mseg/core/issues/56>`_)
- Fix stacktrace output for MATLAB exceptions (`core#59 <https://gitlab.com/h2t/kit-mseg/core/issues/59>`_)
- Made everything locale independent (`core#33 <https://gitlab.com/h2t/kit-mseg/core/issues/33>`_, `core#61 <https://gitlab.com/h2t/kit-mseg/core/issues/61>`_)
- Fix various UI issues (`core#44 <https://gitlab.com/h2t/kit-mseg/core/issues/44>`_, `core#60 <https://gitlab.com/h2t/kit-mseg/core/issues/60>`_, `core#64 <https://gitlab.com/h2t/kit-mseg/core/issues/64>`_)
- Fix a bug where shutting down a Java algorithm would raise an exception

**Features**

- Include ZVC algorithm (`core#25 <https://gitlab.com/h2t/kit-mseg/core/issues/25>`_)
- Allow algorithms to require training data of a certain granularity (`core#27 <https://gitlab.com/h2t/kit-mseg/core/issues/27>`_)
- Add ``mseg`` tool
- Add subcommand to ``mseg`` to generate a C++ skeleton project (`core#35 <https://gitlab.com/h2t/kit-mseg/core/issues/35>`_)
- Add subcommand to ``mseg`` to generate a Python skeleton project (`core#36 <https://gitlab.com/h2t/kit-mseg/core/issues/36>`_)
- Add commands to ``mseg`` to start/stop/restart the core module (`core#37 <https://gitlab.com/h2t/kit-mseg/core/issues/37>`_)
- Add subcommand to ``mseg`` to generate a Java skeleton project (`core#40 <https://gitlab.com/h2t/kit-mseg/core/issues/40>`_)
- Include MATLAB PLI (`core#23 <https://gitlab.com/h2t/kit-mseg/core/issues/23>`_)
- Add subcommand to ``mseg`` to generate a MATLAB skeleton project (`core#42 <https://gitlab.com/h2t/kit-mseg/core/issues/42>`_)
- Improved error handling (`core#47 <https://gitlab.com/h2t/kit-mseg/core/issues/47>`_, `core#49 <https://gitlab.com/h2t/kit-mseg/core/issues/49>`_, `core#50 <https://gitlab.com/h2t/kit-mseg/core/issues/50>`_)
- Refactor algorithm name handling (`core#48 <https://gitlab.com/h2t/kit-mseg/core/issues/48>`_)
- Unify PLI output (`core#45 <https://gitlab.com/h2t/kit-mseg/core/issues/45>`_)
