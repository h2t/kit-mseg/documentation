MSeg Documentation
==================

**MSeg** is a framework for the
`ArmarX <https://armarx.humanoids.kit.edu/index.html>`_ robot development
environment, built to run and evaluate motion segmentation algorithms.

It provides the core functionality to easily integrate motion segmentation
algorithms written in various programming languages. To integrate a motion
segmentation algorithm, a supplied *Programming Language Interface* (PLI) can be
used.

For more information about the structure of MSeg, please refer to the
:doc:`architectural overview <./reference/architectural_overview>`.

.. note:: If you encounter a problem in the documentation or in MSeg, feel free
    to open a new issue in the
    `issue tracker <https://gitlab.com/h2t/kit-mseg/mseg/issues>`_ or send an
    email to incoming+h2t/kit-mseg/mseg@gitlab.com (this will create an issue in
    the issue tracker).


Installation and Setup
----------------------

Install and setup MSeg using the quickstart guide (recommended) or compile it
from source.

- :doc:`Quickstart <../guides/quickstart>` (recommended)
- :doc:`Compile MSeg from Source <../advanced_guides/compile_mseg>`


Integrate a Motion Segmentation Algorithm
-----------------------------------------

Guides on how to integrate a motion segmentation algorithm into the core module.

- :doc:`Integrate a C++ Algorithm <../guides/integrate_cpp_alg>`
- :doc:`Integrate a Java Algorithm <../guides/integrate_java_alg>`
- :doc:`Integrate a Python Algorithm <../guides/integrate_python_alg>`
- :doc:`Integrate a MATLAB Algorithm <../guides/integrate_matlab_alg>`

If you are indecisive about the programming language, we would recommend to use
Python.


Further Reference
-----------------

- :doc:`The mseg* Terminal Tools <../reference/mseg_terminal_tools>`
- :doc:`The MSeg Graphical User Interface <../reference/mseg_gui>`
- :doc:`SegmentationAlgorithm Interface <../reference/segmentation_algorithm_interface>`
- :doc:`DataExchangeProxy Interface <../reference/data_exchange_proxy_interface>`


Developer Guides
----------------

- :doc:`Compile MSeg from Source <../advanced_guides/compile_mseg>`
- :doc:`Notes on Continouos Integration <../advanced_guides/ci>`


Publication
-----------

- `PDF version of the paper <http://h2t.anthropomatik.kit.edu/pdf/Dreher2017.pdf>`_
- BibTeX:

.. code-block:: latex

    @INPROCEEDINGS {Dreher2017,
        author = {Christian R. G. Dreher and Nicklas Kulp and Christian Mandery and Mirko W\"achter and Tamim Asfour},
        title = {A Framework for Evaluating Motion Segmentation Algorithms},
        booktitle = {IEEE/RAS International Conference on Humanoid Robots (Humanoids)},
        pages = {83--90},
        year = {2017},
    }


License
-------

The source code of the MSeg framework is licensed under the
:doc:`GPL 2.0 License <../other/license>`.


.. toctree::
   :maxdepth: 2
   :caption: Guides
   :hidden:

   guides/quickstart
   guides/integrate_cpp_alg
   guides/integrate_java_alg
   guides/integrate_python_alg
   guides/integrate_matlab_alg

.. toctree::
    :maxdepth: 2
    :caption: Advanced Guides
    :hidden:

    advanced_guides/compile_mseg
    advanced_guides/ci

.. toctree::
    :maxdepth: 2
    :caption: Reference
    :hidden:

    reference/architectural_overview
    reference/mseg_terminal_tools
    reference/mseg_gui
    reference/segmentation_algorithm_interface
    reference/data_exchange_proxy_interface
    reference/data_type_mappings

.. toctree::
    :maxdepth: 2
    :caption: Other
    :hidden:

    other/changelog
    other/license
