.depsdeb:
	@echo python3
	@echo python3-pip

.depspip3:
	@echo Sphinx
	@echo sphinx-autobuild
	@echo sphinx-rtd-theme

clean:
	@rm -r ./build 2> /dev/null || true

install:
	sphinx-build -M html ./source ./build

watch:
	sphinx-autobuild ./source ./build/html
